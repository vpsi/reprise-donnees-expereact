.DELETE_ON_ERROR:

.PHONY: all
all:
	$(MAKE) data-pull
	$(MAKE) $(ALL_GENERATED_CSVS) $(ALL_GENERATED_SQLS)
	$(MAKE) data/out/expereact.sql.gz
	$(MAKE) data-push

LATEST_EXPEREACT_DUMP = data/expereact/export-2020-01-06.txt
LATEST_EXPEREACT_DUMP_ENCODING = :encoding(utf-8)

###########################################################################

SQLITE=sqlite3

###########################################################################

-include .Makefile.deps

define mkdir_parent
	@mkdir $(dir $@) 2>/dev/null || true
endef

ALL_GENERATED_CSVS += data/tmp/stock.csv
data/tmp/stock.csv: $(LATEST_EXPEREACT_DUMP)
	$(mkdir_parent)
	time ./scripts/clean-expereact.pl $< "$(LATEST_EXPEREACT_DUMP_ENCODING)" > $@

ALL_GENERATED_CSVS += data/tmp/produits.csv
data/tmp/produits.csv: data/expereact/inventaire\ 2019\ BCH2.xlsx
	$(mkdir_parent)
	./scripts/xlsx2csv.pl "$<" > $@

ALL_GENERATED_CSVS += data/tmp/CAS-BCH1218.csv \
	data/tmp/CAS-CHPH.csv
data/tmp/CAS-%.csv: data/expereact/SAP-%.xlsx
	$(mkdir_parent)
	./scripts/xlsx2csv.pl --make-header-unique $< > $@

ALL_GENERATED_SQLS += data/tmp/stock.sqlite.sql \
	              data/tmp/produits.sqlite.sql \
		      data/tmp/CAS-BCH1218.sqlite.sql \
		      data/tmp/CAS-CHPH.sqlite.sql
data/tmp/%.sqlite.sql: data/tmp/%.csv
	$(mkdir_parent)
	time ./scripts/mk-sql.pl --sqlite $< > $@

ALL_GENERATED_SQLS += data/tmp/stockmaster-sb01.sqlite.sql
data/tmp/stockmaster-sb01.sqlite.sql: data/out/stockmaster-sb01.csv
	$(mkdir_parent)
	time ./scripts/mk-sql.pl --sqlite $< > $@

data/out/expereact.sqlite: $(ALL_GENERATED_SQLS)
	cat $^ | time sqlite3 $@
	@set -e; for table in $$(echo ".tables" | $(SQLITE) $(SQLITE_TARGET)); do \
	if [ 0 = "$$(echo "SELECT COUNT(*) FROM $$table" | $(SQLITE) $@)" ]; \
	then (set -x; echo "DROP TABLE $$table" | $(SQLITE) $(SQLITE_TARGET)); \
	fi; done

data/out/expereact.sql.gz: data/out/expereact.sqlite
	(echo ".dump"; echo ".exit") | $(SQLITE) $< | \
	  gzip -c > "$@"

################################################################

ALL_GENERATED_CSVS += data/out/stockmaster-sb01.csv
data/out/stockmaster-sb01.csv: data/tmp/produits.csv \
                               data/tmp/CAS-BCH1218.csv \
	                       data/tmp/CAS-CHPH.csv
	$(mkdir_parent)
	./scripts/stockmaster-sb01.pl $< \
	     --cas data/tmp/CAS-CHPH.csv \
	     --cas data/tmp/CAS-BCH1218.csv \
	   > $@

################################################################

.PHONY: data-pull
data-pull:
	rsync -zav catalyse-lx-1:/srv/expereact-backup/ data/expereact

.PHONY: data-push
data-push:
	rsync -av data/out/expereact.sql.gz catalyse-lx-1:/srv/expereact/

################################################################

deps_perl := Error Text::CSV_XS Spreadsheet::XLSX Tie::IxHash \
	     Makefile::DOM File::Slurp File::Find File::stat

.PHONY: deps
deps:
	set -x; for dep in $(deps_perl); do cpan $$dep; done

.Makefile.deps: scripts/mkdeps.pl Makefile
	"$<" > "$@"

.PHONY: qc
qc:
	! grep -l Ã© data/tmp/*.csv data/tmp/*.sql
	grep -l é data/tmp/*.csv data/tmp/*.sql

.PHONY: clean
clean:
	rm -rf $(ALL_GENERATED_SQLS) $(ALL_GENERATED_CSVS) $(SQLITE_TARGET) \
	  tmp/* data/expereact/ .Makefile.deps
