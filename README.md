# Expereact extraction

This project consists of an ETL (Extract, Transform, Load) operation
from a bunch of CSV and Excel files, representing the last known state
of the Expereact database, into some SQL that gets [fed into the catalyse-barcodes Web app](https://gitlab.epfl.ch/lhd/ops/-/blob/master/roles/barcode-data/templates/refresh-barcode-data) (after blending with other, more lively data sources).

## `Makefile`

Since the Expereact data set is “cold”, we use a good ole `Makefile`
to process the data set.

```plantuml
participant "Operator workstation" as ws
participant "catalyse-lx-1" as vm
participant "Expereact"

Expereact -> vm: final backup\nin ///srv/expereact-backup//\n(Sep 2019)
vm -> ws: //make data-pull//
ws -> ws: //make all//
ws -> vm: //make data-push//
```
