#!/usr/bin/env perl

=head1 NAME

clean-expereact.pl - Sort out that weird ORIGINAL_stock.TXT format into a real CSV file.

=head1 SYNOPSIS

  clean-expereact.pl ORIGINAL_stock.TXT > stock.csv

=cut

use v5.10;
use strict;
use warnings qw(all);
use utf8;  # in source code
use open qw/:std :utf8/;
use Getopt::Long;
use Pod::Usage qw(pod2usage);
use Text::CSV_XS;

use FindBin qw($Bin);
use lib "$Bin/perllib";
use Expereact::Schema;

my $csv_out = new Text::CSV_XS({ sep_char => "\t" });

our $filter_status;

my $infile = $ARGV[0];
my $txt_in;
open $txt_in, "<$ARGV[1]", $infile or die "$infile: $!";

{
  $_ = <$txt_in>; chomp; s/\r$//;
  # Sometimes the first line is blank, WAT
  redo if ($. == 1) && ! $_;
}

my @header = split m/\t/;
my @header_out = (@header, qw(Qty Unit));
$csv_out->say(\*STDOUT, \@header_out);
my $schema = new Expereact::Schema(@header_out);

my $count = 0;
while(<$txt_in>) {
  chomp;
  my @fields = split m/\t/;
  if (@fields < @header) {
    $_ .= " - " . <$txt_in>;
    chomp;
    redo;
  } elsif (@fields > @header) {
    die "Line too long";
  }

  $count++;

  sanitize_line($schema, \@fields);
  $csv_out->say(\*STDOUT, \@fields);
}

warn "$count lines sans header\n";
exit 0;

##########################################################################

# In a former version of the file, all columns were duplicated
sub check_same_same {
  my$middle = @_ / 2;
  my (@list1) = @_[0..($middle - 1)];
  my (@list2) = @_[$middle..$#_];
  my $join1 = join(' ', @list1);
  my $join2 = join(' ', @list2);
  warn "Mismatch at line $.\n$join1\n$join2\n" unless ($join1 eq $join2);
  return @list2;  # Looks better for that one line at line 492707 that has a mismatch
}

sub sanitize_line {
  my ($schema, $row) = @_;

  foreach my $col ($schema->date_columns) {
    $schema->at($row, $col) = sanitize_date($schema->at($row, $col));
  }
  ($schema->at($row, 'Qty'),
   $schema->at($row, 'Unit')) = parse_quantity($schema->at($row, 'Quantity'));
}

sub sanitize_date {
  my ($date) = @_;
  die "Bad date: $date" unless my ($month, $day, $year) = $date =~ m|(\d+)/(\d+)/(\d+)|;

  if (!($month + $day + $year)) {
    return undef;
  }

  if ($year < 1900) {  # y2k, nous voilââââ
    $year = ($year > 25) ? 1900 + $year : 2000 + $year;
  } elsif ($year =~ m/^20[01](\d\d)$/) {
    $year = "20$1";
  }

  return "$day.$month.$year";
}

sub parse_quantity {
  local $_ = shift;
  my $before = $_;

  s/1[o0]o\b/100/;  # Yes, someone actually typed in "10o g"
  s/^2[.][.]5 l$/2.5 l/;
  s/(\d+)m L$/$1 ml/;
  s/^1kg[.]15[.]30$/1kg/;
  s/10MIL /10000 /;
  s/1[.]2[.]5lt/1.25lt/;

  s/^(?:max[.]?|>|box|pack(?: of)?|case of|cartouche|pck|palette|boite? de|set|rack|lot de|par|pk|pkg|kit|lot|pastilles de)\s*(\d)/$1/;

  debug_rewrite($before, $_);

  if (my ($multiplier, $qty, $unit) = m/^(?:(?:box\s*)?(\d+)\s*(?:x|X|\*|cart[.]|kit à|paq[.]?|bag|box|set de))?\s*([0-9',.]+)\s*(.*?)$/) {
    $qty =~ s/,(\d*$)/.$1/;
    $qty =~ s/[' ]//g;
    $qty *= ($multiplier || 1);

    my $sanitized_unit = sanitize_unit($unit);
    if ($sanitized_unit) {
      debug_rewrite($_, "$qty|$sanitized_unit");
      return ($qty, $sanitized_unit);
    }
  }
  return (undef, undef);
}

sub sanitize_unit {
  local $_ = shift;
  return undef if m/\d|[() ]/;
  return undef if m/^x\b/i;

  s/st\x{0083}ck/stück/gi;
  s/stÌøåÀåÙck/stück/gi;
  s/ÌøåÀåÙ/µ/g;
  s/Î¼/µ/g;
  s/kg¨$/kg/;
  s/^(kg|kilos?)$/kg/i;

  s/[.]$//;

  s/\b(ea|each|test|kit)$/ea./i;
  s/^(u|un|units|untits|untis|pièces?|pieces?|pi\x{008f}ces?|pce?s?|stück|stk)$/piece/i;
  s/^(carton|boites?|boîtes?|bo\x{0089}te|bte?s?|box(?:es|pack|pk)?)\b.*$/box/i;
  s/^(bidon|bt|BID|BTL)$/bottle/i;
  s/^(pack|pck|paq|pk|pak|pkg|pquet|pqts?)$/pack/i;
  s/^(roulx|rouleaux?|rol|roll|rolle|rollen|rou)$/roll/i;
  s/^(cÝnes?|cYnes?|cônes?|cones?)$/cone/i;
  s/^(AMP|Ampulle?|Ampoul|ampoule|vials?)$/vial/i;
  s/^(pounds?|lbs?)$/lb/i;

  s/^(micro-?|u|m|µ|)(l|lts?|ltr|lit|litres?|liters?|g|gr|gram|gramm|gramme)$/sanitize_multiplier($1) . lc(substr($2, 0, 1))/ei;
  s/^$/kg/i;

  s/^M([lg])$/m$1/;

  return $_ || "ea.";
}

sub sanitize_multiplier {
  my ($mult) = @_;
  if ($mult =~ m/^(u$|micro)/i) {
    return "µ";
  } else {
    return $mult;
  }
}

our %seen;
sub debug_rewrite {
  return;  # Comment out when developing
  my ($quantity_orig, $quantity) = @_;
  return unless defined($quantity_orig) && defined($quantity) &&
    $quantity_orig ne $quantity;
  my $msg = "$quantity_orig -> $quantity";
  unless ($seen{$msg}++) {
     warn "($.) $msg\n";
  }
}
