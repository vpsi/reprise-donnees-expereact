#!/usr/bin/env perl

=head1 SYNOPSIS

  mk-sql.pl [ --sqlite ] stock.csv > db.sqlite.sql

=cut


use v5.10;
use strict;
use warnings qw(all);
use open qw/:std :utf8/;
use Getopt::Long;
use Pod::Usage;
use File::Basename qw(fileparse);

use Text::CSV_XS;

use FindBin qw($Bin);
use lib "$Bin/perllib";
use Expereact::Schema;

our $ddl = "DDL::MySQL";
our $dml = "DDL::MySQL";

GetOptions ("sqlite" => sub { $ddl = "DDL::sqlite"; $dml = "DML::sqlite" });

my ($infile) = @ARGV;

open(my $fd_in, "<", $infile) or die "$infile: $!";

chomp($_ = <$fd_in>);
my @hdr = split m/\t/;

my ($basename) = fileparse($infile, qr/\.(tsv|csv)$/i);

my $schema = Expereact::Schema->load($basename, "$Bin/load-expereact.r") ||
  CrudeSchema->from_header($basename, @hdr);
say $ddl->recreate_table($schema);

my $t = new Transactioneer(commit_every => 1000);
for(my $csv_in = new Text::CSV_XS({ sep_char => "\t" });
    my $row = $csv_in->getline($fd_in); ) {
      say $t->line($dml->insert($schema, $row));
}
say $t->done();

exit 0;

package Transactioneer; ####################################################

sub new { my $class = shift; bless { commit_every => 1000, @_ }, $class }

sub line {
  my ($self, $saywhat) = @_;
  my $retval = "";

  if (! exists $self->{commit_counter}) {
    $retval .= "BEGIN;\n";
    $self->{commit_counter} = 0;
  }
  $retval .= $saywhat;

  if (not (++$self->{commit_counter} % $self->{commit_every})) {
    $retval .= $self->done();
  }

  return $retval;
}

sub done {
  my ($self) = @_;
  delete $self->{commit_counter};
  return "COMMIT;\n";
}

package DDL; ##############################################################

sub recreate_table {
  my ($class, $schema) = @_;

  my $tablename = $schema->{table_name};

  my @fields = map {
    sprintf("%s %s", lc($_),
            $class->_sqltype($schema->{types}->{$_}))
  } @{$schema->{columns}};

  return <<"RECREATE_TABLE";
DROP TABLE IF EXISTS $tablename;
CREATE TABLE $tablename (${ \(join(", ", @fields)) });
RECREATE_TABLE
}

package DDL::sqlite;

use base 'DDL';

sub _sqltype {
  my ($class, $rtype) = @_;

  # https://www.sqlite.org/datatype3.html
  if (grep { $rtype eq $_ } qw(date logical)) {
    return "INTEGER";
  } elsif ($rtype eq 'double') {
    return "REAL";
  } else {
    return "TEXT";
  }
}

package DDL::MySQL;

use base 'DDL';

package DML; ##############################################################

sub insert {
  my ($class, $schema, $values_arrayref) = @_;

  my @columns = @{$schema->{columns}};

  return sprintf(
    'INSERT INTO %s (%s) VALUES (%s);',
    $schema->{table_name},
    join(', ', map { lc($_) } @columns),
    join(', ', do {
      my @sql_values;
      for (my $i = 0; $i < @columns; $i++) {
        my $column = $columns[$i];
        my $value = $values_arrayref->[$i];
        if ($value eq '') {
          push @sql_values, 'NULL';
        } else {
          my $rtype = $schema->{types}->{$column};
          my $method = "csv_${rtype}_to_sql";
          push @sql_values, $class->$method($value);
        }
      }
      @sql_values;
    }));
}

sub csv_character_to_sql {
  my ($class, $value) = @_;
  $value =~ s/'/''/g;
  return qq('$value');
}

sub csv_integer_to_sql { $_[$#_] }

sub csv_double_to_sql { $_[$#_] }

sub csv_logical_to_sql {
  my ($class, $value) = @_;
  return $value ? 1 : 0;
}

package DML::sqlite;

use base 'DML';

sub csv_date_to_sql {
  my ($class, $date) = @_;
  # Speed matters here - We cannot afford to use one of these
  # fancy OO parsers from CPAN.
  die "Bad date: $date" unless (my ($d, $m, $y) = $date =~ m|^(\d+)\.(\d+)\.(\d+)$|);
  return "'$y-$m-$d'";
}

package CrudeSchema;  #############################################

sub from_header {
  my ($class, $filename, @header) = @_;

  @header = map { $class->_sanitize_identifier($_) } @header;
  return bless {
    table_name => $class->_sanitize_identifier($filename),
    columns    => [@header],
    types      => { map { $_ => "character" } @header }
  };
}

sub _sanitize_identifier {
  my ($class, $wat) = @_;
  local $_ = $wat;
  s/\x{00e9}/e/g;  # \x{00e9} is "é"
  s/[^A-Za-z0-9]//g;
  return $_;
}
