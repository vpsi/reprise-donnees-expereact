#!/usr/bin/env perl

=head1 NAME

mkdeps.pl - Compute dependencies from the Makefile

=head1 SYNOPSIS

  mkdeps.pl > .Makefile.deps

=head1 DESCRIPTION

Each generated file depends on the Perl script that is used to
generate it, and the dependencies thereof (C<scripts/load-expereact.r>
and everything in C<scripts/perllib/>)

=cut


use v5.10;
use strict;
use warnings qw(all);
use open qw/:std :utf8/;

use MDOM::Document::Gmake;
my $dom = MDOM::Document::Gmake->new('Makefile');

say Expereact::Makefile->parse($dom)->deps;

1;

package Expereact::Makefile; ############################################

sub parse {
  my ($class, $dom) = @_;

  my $self = bless { rules => [], phonies => {} }, $class;

  my (@phonies, @rules, $current_rule);
  foreach my $line ($dom->children) {
    if ($line->isa('MDOM::Rule::Simple')) {
      my @toks = grep { $_->isa('MDOM::Token::Bare') } $line->children();
      next unless @toks >= 1;
      my $target = $toks[0]->content;

      if (@toks == 2 and $target eq ".PHONY") {
        $self->{phonies}->{$toks[1]->content} = 1;
      } else {
        push(@rules, $current_rule = "${class}::Rule"->new($target));
      }
    } elsif ($line->isa('MDOM::Command')) {
      $current_rule->add_command($line);
    }
  }

  $self->{rules} = [grep { $self->_is_real_rule($_) } @rules];
  return $self;
}

sub deps { map { $_->deps } @{shift->{rules}} }

sub _is_real_rule {
  my ($self, $rule) = @_;

  return 0 if $self->{phonies}->{$rule->{target}};

  return 0 if $rule->{target} eq ".DELETE_ON_ERROR";

  return @{$rule->{commands}} > 0;
}

package Expereact::Makefile::Rule;

use File::Slurp qw(read_file);
use File::Find;

sub new {
  my ($self, $target) = @_;
  bless {target => $target, commands => []}, shift
}

sub add_command {
  my ($self, $cmd) = @_;
  push @{$self->{commands}}, $cmd;
  return $self;
}

sub deps {
  my ($self) = @_;

  my @deps;
  local $_;
  for ( map { $_->content } @{$self->{commands}}) {
    if (m|(scripts/\S+)|) {
      push @deps, $self->_analyze_deps($1);
    }
  }

  if (@deps) {
    return sprintf("%s: %s\n", $self->{target}, join(" ", @deps));
  } else {
    return ''
  }
}

sub _analyze_deps {
  my ($self, $script) = @_;

  my @deps = $script;
  local $_ = read_file($script);
  if (m/Expereact::Schema/) {
    push @deps, $self->_all_perllibs, $self->_load_expereact_r;
  }
  return @deps;
}

sub _all_perllibs {
  my @perllibs;
  find(sub { push @perllibs, $File::Find::name if (-f $_); },
       "scripts/perllib");
  return @perllibs;
}

sub _load_expereact_r { 'scripts/load-expereact.r' }
