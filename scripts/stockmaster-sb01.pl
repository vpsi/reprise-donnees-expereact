#!/usr/bin/env perl

=head1 NAME

stockmaster-sb01.pl - Prepare the so-called "stock master items" CSV
file for ingestion

=head1 SYNOPSIS

  stockmaster-sb01.pl [--cas <cas.csv> ...] produits.csv > stockmaster.csv

=cut


use v5.10;
use strict;
use warnings qw(all);
use open qw/:std :utf8/;
use Getopt::Long;
use Pod::Usage;
use File::Basename qw(fileparse);

use Text::CSV_XS qw(csv);
use Tie::IxHash;

our %CAS;

GetOptions ( "cas=s" => sub { CAS->load(@_) } ) or pod2usage(1);

my ($infile) = @ARGV;

my $csv_out = new Text::CSV_XS({ sep_char => "\t" });

tie(my %columns, 'Tie::IxHash',
    "Stock Master Name"       => "Stock Master SB01",
    "Stock Master Item Name"  => sub { $_->{Description} },
    "Customer Catalog #"      => sub { $_->{reference} },
    "Active"                  => "Y",
    "Product Bar Code"        => sub { $_->{bottle_number} },
    "Print Labels"            => undef,
    "Stock As"                => sub { unit2stock_as($_->{unit}) },
    "Description"             => sub { $_->{Description} },
    "Label Name"              => sub { $_->{Description} },
    "Brand"                   => sub { $_->{supplier_code} },
    "Manufacturer Part #"     => undef,
    "Stock Price"             => sub { currency($_->{price}) },
    "UNSPSC #"                => undef,
    "Commodity Code"          => undef,
    "Product Category"        => sub {
      ( $_->{category} && ($_->{category} =~ m/^(material|gas)$/) ) ?
        "Chemical"                                                  :
        "Other"
    },
    "Supplier"                => sub { $_->{supplier_code} },
    "Supplier Catalog #"      => sub { $_->{reference} },
    "Supplier Price"          => sub { currency($_->{supplier_price}) },
    "Supplier Currency"       => sub { normalize_currency_name($_->{currency}) },
    "Stocking Units Per Catalog Item"
                              => undef,  # TODO: maybe we want to extract the quantity
                                         # as the digit parts of $_->{unit} here
    "Default EOQ"             => sub { $_->{reorder_qty} },
    "Default Reorder PT"      => sub { $_->{minimal_stock} },
    "Default Stock Inv Min"   => undef,
    "Default Stock Inv Max"   => undef,
    "SpendDirector SKU"       => undef,
    "Material Name"           => sub { CAS->lookup($_) },
    "Package Quantity"        => sub { CAS->lookup($_) ? (split_unit($_->{unit}))[0] : undef },
    "Package Unit"            => sub { CAS->lookup($_) ? (split_unit($_->{unit}))[1] : undef },
    "Packages Per Stock Unit" => sub { CAS->lookup($_) ? 1   : undef },
    "Create Containers"       => sub { CAS->lookup($_) ? "Y" : undef },
    "Custom1"                 => undef,
    "Custom2"                 => undef,
    "Replenishment Note"      => undef,
   );

$csv_out->say(\*STDOUT, [keys(%columns)]);
foreach my $product (@{csv(in => $infile, headers => "auto", sep_char => "\t")}) {
  my %product = map { $_ => trim($product->{$_}) } (keys %$product);

  my @values;
  foreach my $column (keys(%columns)) {
    my $val = $columns{$column};
    if (ref($val) eq "CODE") {
      local $_ = {%product};
      $val = $val->();
    }
    push @values, $val;
  }

  $csv_out->say(\*STDOUT, [@values]);
}


exit(0);  ############################################################################

sub trim {
  local $_ = shift;
  s/^\s+//;
  s/\s+$//;
  return length ? $_ : undef;
}

sub split_unit {
  my ($unit) = @_;
  if (! $unit) { return qw(1 EA); }
  $unit = uc($unit);

  my ($q, $u);
  unless (($q, $u) = $unit =~ m/(\d+)\s*([A-Z]+)$/) {
    return qw(1 EA);
  }
  return ($q, unit2stock_as($u));
}

sub unit2stock_as {
  my ($unit) = @_;
  if (! $unit) { return "EA"; }
  $unit = uc($unit);
  unless (($unit) = $unit =~ m/([A-Z]+)$/) {
    return "EA";
  }

  my %map =
    ("AMP"    => "AN",
     "BID"    => "CAN",
     "CAR"    => "PACK",
     "JER"    => "CAN",
     "PAI"    => "EA",
     "PAQ"    => "BOX",
     "PLAQUE" => "PCE",
     "ROU"    => "ROL",
     "SAC"    => "PACK",
     "SET"    => "EA");
  return $map{$unit} || "EA";
}

sub normalize_currency_name {
  local $_ = shift;
  if (! $_) {
    return undef;
  } elsif (m/CH/) {
    return "CHF";
  } elsif (m/EUR/) {
    return "EUR";
  } else {
    return undef;
  }
}

sub currency {
  my ($amount) = @_;
  $amount =~ s/,/./;

  return sprintf("%.2f", $amount);
}

package CAS; #######################################################################

use vars qw(%cas);  # Two-level hash of hashes, first key is the _canon_location,
                    # second key is the bar code (aka bottle_number)
use Text::CSV_XS qw(csv);
use Carp qw(croak);

sub load {
  my ($class, $unused_getopt_long_somethingsomething, $infile) = @_;

  my ($loc_raw) = $infile =~ m/CAS-(.*)\.csv/;
  croak "Cannot guess location from file name: $infile" unless
    (my $loc = $class->_canon_location($loc_raw));

  local $_;
  foreach (@{csv(in => $infile, headers => "auto", sep_char => "\t",
                 encoding => "UTF-8")})
  {
    my $barcode = $_->{bottle_number};
    next unless my $cas = $_->{"CAS-numbre"};    # Not a typo
    if (exists($cas{$loc}{$barcode})) {
      die "Duplicate CAS found in $loc: $cas (for barcodes $barcode and $cas{$loc}{$barcode})";
    } else {
      $cas{$loc}{$barcode} = $cas;
    }
  }
}

sub lookup {
  my ($class, $record) = @_;

  return unless my $loc_long = $record->{localisation};
  return unless my $loc = $class->_canon_location($loc_long);

  return $cas{$loc}{$record->{bottle_number}};
}

sub _canon_location {
  local $_; my $class; ($class, $_) = @_;
  if (m/^CH/) {
    return "CH";
  } elsif (m/^BCH/) {
    return "BCH";
  } else {
    return undef;
  }
}
