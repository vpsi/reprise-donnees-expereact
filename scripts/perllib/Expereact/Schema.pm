package Expereact::Schema;

use strict;
use warnings qw(all);
use File::Basename qw(dirname);
use Fatal qw(open);

sub new {
  my ($class, @columns) = @_;
  return bless {
    columns => \@columns,
    offsets => { reverse( map { $_ => $columns[$_] } (0..$#columns) ) },
    _columns_matching_cache => {},
  }, $class;
}

sub load {
  my ($class, $table_name, $path) = @_;

  return unless defined(my $tabledef =
                       $class->_parse_schema_from_R($path)->{$table_name});
  my $self = $class->new(@{$tabledef->{columns}});
  $self->{table_name} = $table_name;
  $self->{types} = {%{$tabledef->{types}}};
  return $self;
}

sub length {
  my ($self) = @_;
  return scalar(@{$self->{columns}});
}

sub at :lvalue {
  my ($self, $row, $field_name) = @_;
  return @$row[$self->{offsets}->{$field_name}];
}

sub _columns_matching {
  my ($self, $re) = @_;
  if (! $self->{_columns_matching_cache}{"$re"}) {
    $self->{_columns_matching_cache}{"$re"} = [grep { $_ =~ $re } @{$self->{columns}}];
  }
  return @{$self->{_columns_matching_cache}{"$re"}};
}

sub date_columns { shift->_columns_matching(qr/date/); }

sub _parse_schema_from_R {
  my ($cls, $path) = @_;

  open(my $r_schema, "<", $path);

  local $_;
  my $schema = {};
  my $table;
  while(<$r_schema>) {
    if (my ($next_table) = m/   (\w+)     \s*    \Q<-\E    /x) {
      $table = $next_table;
    } elsif (my ($column, $type) = m/ (\w+) \s* = \s* col_(\w+)/x) {
      push @{$schema->{$table}->{columns}}, $column;
      $schema->{$table}->{types}->{$column} = $type;
    }
  }

  return $schema;
}

1;
