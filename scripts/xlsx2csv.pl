#!/usr/bin/env perl

=head1 SYNOPSIS

  xlsx2csv.pl [ --make-header-unique ] from.xls > to.csv

=cut

use v5.10;
use strict;
use warnings qw(all);
use open qw/:std :utf8/;
use utf8;

use Getopt::Long;
use Pod::Usage;

our $fix_header;

GetOptions ("make-header-unique" => \$fix_header) or pod2usage(1);

use Spreadsheet::XLSX;
use Text::CSV_XS;

my $xls = do {
  local $SIG{__WARN__} = sub {};
  Spreadsheet::XLSX->new($ARGV[0]);
};

my @worksheets = @{$xls -> {Worksheet}};
die sprintf("This script works only with XLSX files "
              . "that have only one worksheet (found "
              . scalar(@worksheets) . ")")
  unless @worksheets == 1;
my ($sheet) = @worksheets;

my $csv_out = new Text::CSV_XS({ sep_char => "\t", auto_diag => 2 });

$sheet->{MaxRow} ||= $sheet->{MinRow};
my $firstline = 1;
foreach my $row ($sheet->{MinRow} .. $sheet->{MaxRow}) {
  $sheet->{MaxCol} ||= $sheet->{MinCol};
  my @row = map {
    my $cell_obj = $sheet->{Cells}[$row][$_];

    if (! defined($cell_obj->{Val})) {
      undef;
    } else {
      my $cell = ($cell_obj->{Type} eq 'Numeric' ?
                    "" . $cell_obj->{_Value}     :
                    $cell_obj->{Val});
      utf8::decode($cell) or do {
        require Data::Dumper;
        die Data::Dumper::Dumper($cell_obj);
      };
      $cell;
    }
  } ($sheet->{MinCol} ..  $sheet->{MaxCol});

  if ($fix_header && $firstline) {
    my %taken;
    @row = map {
      my $substituted;
      for(my $suffix = ""; do { $substituted = "$_$suffix"; $taken{$substituted} };
          $suffix++) {}
      $taken{$substituted} = 1;
      $substituted
    } @row;
    undef $firstline;
  }

  $csv_out->say(\*STDOUT, \@row);
}

sub trim {
  local $_ = shift;
  return undef if ! defined;
  s/^\s+//;
  s/\s+$//;
  return length ? $_ : undef;
}
